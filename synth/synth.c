#include "MidiEvent.h"
#include <stdint.h>

#include <stdlib.h>

typedef struct _Synth_t
{
	uint32_t sampleRate;

	float gain;
	float freq;

} Synth_t;

Synth_t state;

void synth_init(uint32_t sampleRate)
{
	state.sampleRate = sampleRate;
	state.gain = 0.f;
	state.freq = 0.f;
}

void synth_event(MidiEvent_t* e)
{
	switch (e->met)
	{
	case MET_NoteOn:
		state.gain = 1.f;
		break;
	case MET_NoteOff:
		state.gain = 0.f;
		break;
	}
}

void synth_render(float* left, float* right, long bufferLen)
{
	long i;
	float sample;

	for (i = 0; i < bufferLen; i++)
	{
		sample = ((rand() / (float)RAND_MAX) * 2.f) - 1.f;

		sample *= state.gain;

		*left++ = sample;
		*right++ = sample;
	}
}