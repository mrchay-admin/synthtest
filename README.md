# SynthTest, the almost mostly framework-less synth testing framework #

A super simple synth testing framework for Windows.

### So you wake up one afternoon and you want to write a new synth... ###

* You're on windows (yes dad, open source is healthier, but sometimes you want a chips and gravy sandwich)
* You want to write it in C (or C++ if you're feeling fruity)
* Visual Studio is actually pretty neat, its free, and provides great optimisation tools
* You cant be bothered to remember how to hook up your midi controller
* Wouldnt it be great if you could just write code, hit a key, and bang, hear what it sounds like.
* Sure, one day you can maybe make it into a VST.. But for now:

**It's the journey that matters**

### How do I get set up? ###

0. Dig out midi controller from loft, and plug it in
1. Fork project.
2. Open in Visual Studio 2015
3. Build and run, hit a note
4. Damn, white noise at 100% isn't good. Curse developer, plug bleeding ears with toilet paper.
5. Replace synth/synth.c with own masterpiece

### Implementing synth.c ###

The point of this framework is that it is super-lite. When you have written your synth (well, more likely when you have tested the basic principle of your new methods out), you can easily lift'n'shift them into anything else. I use this framework for developing embedded instruments as well as VST style plugins.

All that is required is to implement the following 3 functions. Typically you would do this in synth/synth.c (a cosy subdirectory where you can keep all your bits and bobs):

```
#!c
// called once, with the sample rate.
void synth_init(uint32_t sampleRate)
{
  // this is a place for you to load data, and pre-compute stuff.
}
```

```
#!c
// called on any midi event.
void synth_event(MidiEvent_t* e)
{
  // use a switch statement or something similar
  // this is the only 'framework-y' bit: you need to read data from
  // the MidiEvent_t struct. its self explanatory, and is only provided
  // so you dont have to parse raw midi.

  switch (e->met)
  {
    case MET_NoteOn:
      // do something useful, probably store state somewhere
    break;
    case MET_NoteOff:
      // do something useful, again you probably want to store state somewhere.
    break;
  }
}
```

```
#!c
void synth_render(float* left, float* right, long bufferLen)
{
  // couldnt be easier: just set bufferLen samples into left and right buffers.
  // any state you are relying on will be correct, because your synth_event() func
  // has been called, sample accurately, when needed.
  // you will need to have stored some state globally so you can refer to it in this function.
}
```

### And thats about it. ###

A very terrible implementation of a keyboard-keyboard is thrown in as well, which allows basic testing even if you dont have a midi keyboard connected.


