#ifndef DEQUE_H
#define DEQUE_H

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

typedef struct Deque_t Deque_t;
typedef struct DequeItem_t DequeItem_t;

Deque_t* Deque_create();
void Deque_destroy(Deque_t* t);

void Deque_pushBack(Deque_t* t,void* p);
void Deque_pushFront(Deque_t* t, void* p);

void* Deque_popBack(Deque_t* t);
void* Deque_popFront(Deque_t* t);

void* Deque_at(Deque_t* t, uint32_t i);
bool Deque_contains(Deque_t* t, void* p);

DequeItem_t* Deque_front(Deque_t* t);
DequeItem_t* Deque_back(Deque_t* t);
DequeItem_t* Deque_next(DequeItem_t* iter);
DequeItem_t* Deque_prev(DequeItem_t* iter);
void* Deque_itemDeref(DequeItem_t* iter);

size_t Deque_size(Deque_t* t);

void* Deque_toArray(Deque_t* t);

void Deque_appendDeque(Deque_t* t, Deque_t* other);

#endif