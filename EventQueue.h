#ifndef R_MESSAGE_QUEUE
#define R_MESSAGE_QUEUE

#include "MidiEvent.h"

typedef struct EventQueue_t EventQueue_t;

EventQueue_t* EventQueue_create(uint32_t maxMessageCount);
void EventQueue_destroy(EventQueue_t* t);

void EventQueue_push(EventQueue_t* t, MidiEvent_t* msg);
MidiEvent_t* EventQueue_pop(EventQueue_t* t);
size_t EventQueue_size(EventQueue_t* t);
void EventQueue_clear(EventQueue_t* t);

void EventQueue_normaliseTimestamps(EventQueue_t* t,uint32_t start, uint32_t duration, uint32_t samples);

MidiEvent_t* EventQueue_front(EventQueue_t* t);
MidiEvent_t* EventQueue_next(EventQueue_t* t, MidiEvent_t* e);


#endif
