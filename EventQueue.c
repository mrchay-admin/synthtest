
#include "EventQueue.h"

#include <stdlib.h>
#include <string.h>

struct EventQueue_t
{
	MidiEvent_t* buf;
	MidiEvent_t* wp;
	MidiEvent_t* rp;
	MidiEvent_t* tail;
	size_t count;
};

EventQueue_t* EventQueue_create(uint32_t maxMessageCount)
{
	EventQueue_t* t = malloc(sizeof(EventQueue_t));
	memset(t, 0, sizeof(EventQueue_t));
	t->buf = malloc(sizeof(MidiEvent_t)*maxMessageCount);
	memset(t->buf, 0, sizeof(MidiEvent_t)*maxMessageCount);
	t->wp = t->buf;
	t->rp = t->buf;
	t->tail = &t->buf[maxMessageCount];
	t->count = 0;

	return t;
}

void EventQueue_clear(EventQueue_t* t)
{
	t->wp = t->buf;
	t->rp = t->buf;
	t->count = 0;
}

void EventQueue_destroy(EventQueue_t* t)
{
	free(t->buf);
	free(t);
}

void EventQueue_push(EventQueue_t* t,MidiEvent_t* msg)
{
	memcpy(t->wp,msg,sizeof(MidiEvent_t));
	t->wp++;
	if (t->wp == t->tail) 
		t->wp = t->buf;

	t->count++;
}

MidiEvent_t* EventQueue_pop(EventQueue_t* t)
{
	if (t->count > 0) 
	{
		MidiEvent_t* ret = t->rp;
		t->rp++;
		t->count--;
		if (t->rp == t->tail)
			t->rp = t->buf;
		return ret;
	}
	return 0;
}

size_t EventQueue_size(EventQueue_t* t)
{
	return t->count;
}

void EventQueue_normaliseTimestamps(EventQueue_t* t,uint32_t start, uint32_t duration, uint32_t samples)
{
	MidiEvent_t* p = t->rp;
	unsigned int c = t->count;

	while (c--)
	{
		MidiEvent_normalizeTimestamp(p, start, duration, samples);
		p++;
		if (p == t->tail)
			p = t->buf;
	}
}

MidiEvent_t* EventQueue_front(EventQueue_t* t)
{
	return t->rp;
}

MidiEvent_t* EventQueue_next(EventQueue_t* t, MidiEvent_t* e)
{
	e++;
	if (e == t->tail)
		e = t->buf;
	return e;
}




