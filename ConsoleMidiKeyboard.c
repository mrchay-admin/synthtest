
#include <Windows.h>
#include <stdbool.h>
#include "ConsoleMidiKeyboard.h"
#include <stdio.h>

void ConsoleKeyboard(midiEventCb cb, void* tag)
{
	DWORD cNumRead, i;
	INPUT_RECORD irInBuf[128];
	HANDLE hStdin;
	char* pkey;
	int oct = 2;
	bool noteState[256];
	memset(noteState, 0, sizeof(bool) * 256);

	static char* keys = "q2w3er5t6yu8i9o0p";

	printf("console keyboard\n");
	printf(" 2 3   5 6   8 9 0\n");
	printf("q w e r t y u i o p");

	// get input handle and set console mode such that we can recieve keyboard events
	hStdin = GetStdHandle(STD_INPUT_HANDLE);
	SetConsoleMode(hStdin, ENABLE_WINDOW_INPUT);

	// loop forever
	for (;;)
	{
		// read as many events as we can
		ReadConsoleInput(
			hStdin,      // input buffer handle 
			irInBuf,     // buffer to read into 
			128,         // size of read buffer 
			&cNumRead); // number of records read 

						// loop over events we got
		for (i = 0; i < cNumRead; i++)
		{
			// is this a key event
			if (irInBuf[i].EventType == KEY_EVENT)
			{
				// get the event details
				KEY_EVENT_RECORD ker = irInBuf[i].Event.KeyEvent;

				// is this key part of our shitty keyboard
				pkey = strchr(keys, (int)ker.uChar.AsciiChar);
				if (pkey && ker.uChar.AsciiChar != 0)
				{
					unsigned char m[3];

					m[0] = 0x90;
					m[1] = oct * 12 + (69 - 48) + pkey - keys;
					m[2] = ker.bKeyDown ? 100 : 0;

					if (m[1] > 127) m[1] = 127;

					if (ker.bKeyDown == !noteState[ker.uChar.AsciiChar])
					{
						cb(m, 3, tag);
						noteState[ker.uChar.AsciiChar] = ker.bKeyDown;
					}
				}

				// handle other keypresses
				if (!ker.bKeyDown)
				{
					switch (ker.uChar.AsciiChar)
					{
					case '[': oct = (oct > 0) ? oct - 1 : 0; break;
					case ']': oct = (oct < 6) ? oct + 1 : 6; break;
					}

					// escape returns from loop
					if (ker.wVirtualKeyCode == VK_ESCAPE)
						return;
				}
			}
		}
	}
}