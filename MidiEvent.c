#include "MidiEvent.h"

bool MidiEvent_parseRaw(MidiEvent_t* e, uint8_t* data, size_t len)
{
	// message header looks like sMMMcccc
	// s is status bit (should be 1)
	// MMM is message
	// cccc is channel
	if (len == 3 && data[0] & (1 << 7))
	{
		uint8_t channel = data[0] & 0x0f;		// channel is next 3 bits
		uint8_t msg = (data[0] & 0x70) >> 4;

		switch (msg)
		{
		default: break;
		case 0:	// note off
			e->met = MET_NoteOff;
			e->data1 = data[1];	// pitch
			e->data2 = data[2];	// velocity
			e->channel = channel;
			return true;
		case 1:	// note on
			e->met = data[2] ? MET_NoteOn : MET_NoteOff; // zero velocity means note-off
			e->data1 = data[1];	// pitch
			e->data2 = data[2];	// velocity
			e->channel = channel;
			return true;
		case 3: // controller
			e->met = MET_Controller;
			e->data1 = data[1];	// controller
			e->data2 = data[2];	// value
			e->channel = channel;
			return true;
		}
	}
	return false;
}

void MidiEvent_normalizeTimestamp(MidiEvent_t* e, uint32_t start, uint32_t duration, uint32_t samples)
{
	uint32_t newTimestamp;

	if (duration == 0)
	{
		newTimestamp = 0;
	}
	else
	{
		newTimestamp = ((e->timestamp - start) * samples) / duration;

		if (newTimestamp >= samples)
		{
			newTimestamp = samples - 1;
		}
	}

/**/printf("%d: %d-->%d\n",start, e->timestamp, newTimestamp);

	e->timestamp = newTimestamp;
}


