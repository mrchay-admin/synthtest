#include "WinMidiServer.h"
#include "PortAudioServer.h"
#include <Windows.h>
#include "EventQueueBuffer.h"
#include "ConsoleMidiKeyboard.h"

#define AUDIOBUFSIZE		512
#define AUDIOSAMPLERATE		44100

EventQueueBuffer_t* eqb;

#define NOW() ((uint32_t)GetTickCount())

extern void synth_event(MidiEvent_t* e);
extern void synth_render(float* left, float* right, long bufferLen);
extern void synth_init(uint32_t sampleRate);

void renderCb(float* left, float* right, long bufferLen, void* privateTag)
{
	uint32_t now = NOW();
	static uint32_t then = 0;

	if (then == 0)
	{
		then = now;
		return;
	}

	EventQueue_t* q = EventQueueBuffer_flip(eqb);
	EventQueue_normaliseTimestamps(q, then, now - then, bufferLen);
	
	MidiEvent_t* e;
	int32_t c;
	int32_t sectionStart = 0;
	int32_t sectionEnd = 0;

	c = EventQueue_size(q);
	e = c ? EventQueue_front(q) : NULL;

	// process all sections
	do
	{
		// this section starts where we got up to last time
		sectionStart = sectionEnd;

		// process all events on this sample
		while (c && e && 			// messages available
			e->timestamp == sectionStart)	// event is for current sample
		{
			// inform module
			synth_event(e);

			// get next message, or set pointer to 0
			c--;
			if (c <=0)
				e = NULL;
			else 
				e =	EventQueue_next(q, e);
		}

		// section ends at next message, or end of block
		if (e != NULL)
		{
			sectionEnd = e->timestamp;

			if (sectionEnd - sectionStart > 0)
			{
				uint32_t sampleCount = sectionEnd - sectionStart;

				if (sampleCount < AUDIOBUFSIZE)
				{
					synth_render(left+sectionStart,right+sectionStart, sampleCount);
				}
			}
		}

	} while (e != NULL);

	// any left?
	if (sectionEnd != bufferLen)
		synth_render(left+sectionEnd,right+sectionEnd, bufferLen - sectionEnd);

	EventQueue_clear(q);

	then = now;
}

void midiCb(uint8_t* data, size_t len, void* privateTag)
{
	MidiEvent_t m;
	if (MidiEvent_parseRaw(&m, data, len))
	{
		EventQueue_t* eq = EventQueueBuffer_getBuffer(eqb);
		m.timestamp = NOW();
		EventQueue_push(eq, &m);
	}
}

int main()
{
	eqb = EventQueueBuffer_create(128);
	synth_init(AUDIOSAMPLERATE);

	WinMidiServer_init(midiCb, NULL);
	WinMidiServer_startDevice(0);

	PortAudioServer_init(renderCb, 0, AUDIOBUFSIZE, AUDIOSAMPLERATE);

	// stays here in loop
	ConsoleKeyboard(midiCb,NULL);

	WinMidiServer_destroy();
	PortAudioServer_stop();

	EventQueueBuffer_destroy(eqb);
}

