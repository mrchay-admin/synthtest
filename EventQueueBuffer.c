
#include "EventQueueBuffer.h"
#include "EventQueue.h"
#include <stdint.h>

#include <stdlib.h>
#include <string.h>

struct EventQueueBuffer_t
{
	EventQueue_t* queues[2];
	uint32_t current;
};

EventQueueBuffer_t* EventQueueBuffer_create(uint32_t bufferSize)
{
	EventQueueBuffer_t* t = malloc(sizeof(EventQueueBuffer_t));
	memset(t, 0, sizeof(EventQueueBuffer_t));
	
	t->queues[0] = EventQueue_create(bufferSize);
	t->queues[1] = EventQueue_create(bufferSize);
	t->current = 0;
	return t;
}

void EventQueueBuffer_destroy(EventQueueBuffer_t* t)
{
	EventQueue_destroy(t->queues[0]);
	EventQueue_destroy(t->queues[1]);
	free(t);
}

EventQueue_t* EventQueueBuffer_getBuffer(EventQueueBuffer_t* t)
{
	return t->queues[t->current];
}

EventQueue_t* EventQueueBuffer_flip(EventQueueBuffer_t* t)
{
	// should be atomic swap!
	EventQueue_t* tmp;
	tmp = t->queues[t->current];
	t->current = ( (~t->current) & 1);
	EventQueue_clear(t->queues[t->current]);
	return tmp;
}
