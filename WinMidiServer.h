#ifndef WinMidiServer_h
#define WinMidiServer_h
#ifdef WIN32

#include <stdint.h>
#include <stdbool.h>

typedef void(*WinMidiServer_cb) (uint8_t* data,size_t len, void* privateTag);

void WinMidiServer_init(WinMidiServer_cb cb,void* privateTag);
void WinMidiServer_destroy();

// list devices
size_t WinMidiServer_getInputDeviceCount();
char*  WinMidiServer_getInputDeviceName();

// start getting callbacks from specified device
bool WinMidiServer_startDevice(uint32_t i);
void WinMidiServer_stopDevice();

#endif
#endif
