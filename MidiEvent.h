#ifndef MIDI_EVENT_INCLUDE
#define MIDI_EVENT_INCLUDE

#include <stdint.h>
#include <stdbool.h>

// event types which we care about
typedef enum _MidiEventType_e
{
	MET_NoteOn = 0,
	MET_NoteOff,
	MET_PitchBend,
	MET_Controller
} MidiEventType_e;

typedef struct _MidiEvent_t
{
	MidiEventType_e met;

	uint8_t channel;

	uint32_t data1;
	uint32_t data2;

	uint32_t timestamp;

} MidiEvent_t;

// parse raw data from a midi device into a message
bool MidiEvent_parseRaw(MidiEvent_t* e, uint8_t* data, size_t len);

void MidiEvent_normalizeTimestamp(MidiEvent_t* e, uint32_t start, uint32_t duration, uint32_t samples);

#endif

