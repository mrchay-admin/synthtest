#include "PortAudioServer.h"

#include <stdio.h>
#include "portaudio.h"
#pragma comment(lib, "portaudio/portaudio_x86.lib")

static void* pri;

static int patestCallback(const void *inputBuffer, void *outputBuffer,
	unsigned long framesPerBuffer,
	const PaStreamCallbackTimeInfo* timeInfo,
	PaStreamCallbackFlags statusFlags,
	void *userData)
{
	/* Cast data passed through stream to our structure. */
	PortAudioServer_cb cb = (PortAudioServer_cb)userData;

	float *left = ((float **)outputBuffer)[0];
	float *right = ((float **)outputBuffer)[1];
	(void)inputBuffer;

	cb(left, right, framesPerBuffer, pri);

	return 0;
}

static PaStream *stream;

void PortAudioServer_cpu(void)
{
	printf("CPU %f\n", Pa_GetStreamCpuLoad(stream));
}

void PortAudioServer_init(PortAudioServer_cb cb, void* ptag, uint32_t blockSize, uint32_t sampleRate)
{
	PaError err = Pa_Initialize();
	if (err != paNoError) goto error;

	PaStreamParameters outputParameters;
	pri = ptag;

	outputParameters.device = Pa_GetDefaultOutputDevice(); /* default output device */
    if (outputParameters.device == paNoDevice) 
	{
      printf("Error: No default output device.\n");
      goto error;
	}
    outputParameters.channelCount = 2;       /* stereo output */
    outputParameters.sampleFormat = paFloat32 | paNonInterleaved; /* 32 bit floating point output */
    outputParameters.suggestedLatency = Pa_GetDeviceInfo(outputParameters.device)->defaultLowOutputLatency;
    outputParameters.hostApiSpecificStreamInfo = NULL;

    err = Pa_OpenStream(&stream,
						NULL, /* no input */
						&outputParameters,
						sampleRate,
						blockSize,
						paClipOff,      /* we won't output out of range samples so don't bother clipping them */
						patestCallback,
						cb);
    if (err != paNoError) goto error;

    err = Pa_StartStream(stream);
    if (err != paNoError) goto error;

	return;
error:
	printf("PortAudio error: %s\n", Pa_GetErrorText(err));
}

void PortAudioServer_stop(void)
{
	PaError err = Pa_StopStream(stream);
	if (err != paNoError) goto error;

	err = Pa_CloseStream(stream);
	if (err != paNoError) goto error;

	err = Pa_Terminate();
	if (err != paNoError) goto error;

	return;
error:
	printf("PortAudio error: %s\n", Pa_GetErrorText(err));
}