#include "Deque.h"

typedef struct DequeItem_t DequeItem_t;
struct DequeItem_t
{
	void* p;
	DequeItem_t* next;
	DequeItem_t* prev;
};

struct Deque_t
{
	DequeItem_t* front;
	DequeItem_t* back;
	size_t size;
};

Deque_t* Deque_create()
{
	Deque_t* t = malloc(sizeof(Deque_t));
	memset(t, 0, sizeof(Deque_t));
	return t;
}

void Deque_destroy(Deque_t* t)
{
	DequeItem_t* i, *del;

	i = t->front;
	while (i)
	{
		del = i;
		i = i->next;
		free(del);
	}

	free(t);
}

static DequeItem_t* _newItem(void* p)
{
	DequeItem_t* i = malloc(sizeof(DequeItem_t));
	memset(i, 0, sizeof(DequeItem_t));
	i->p = p;
	return i;
}

static void* _delItem(DequeItem_t * i)
{
	void* p = i->p;
	free(i);
	return p;
}

void Deque_pushBack(Deque_t* t, void* p)
{
	DequeItem_t* i = _newItem(p);

	if (t->back == NULL)
		t->front = t->back = i;
	else
	{
		i->prev = t->back;
		t->back->next = i;
		t->back = i;
	}

	t->size++;
}

void Deque_pushFront(Deque_t* t, void* p)
{
	DequeItem_t* i = _newItem(p);

	if (t->front == NULL)
		t->front = t->back = i;
	else
	{
		i->next= t->front;
		t->front->prev = i;
		t->front = i;
	}

	t->size++;
}

void* Deque_popBack(Deque_t* t)
{
	if (t->size == 0) return NULL;
	else if (t->size == 1)
	{
		DequeItem_t* i = t->back;
		t->front = t->back = NULL;
		t->size = 0;
		return _delItem(i);
	}
	else
	{
		DequeItem_t* i = t->back;
		t->back = i->prev;
		t->size--;
		return _delItem(i);
	}
}
void* Deque_popFront(Deque_t* t)
{
	if (t->size == 0) return NULL;
	else if (t->size == 1)
	{
		DequeItem_t* i = t->front;
		t->front = t->back = NULL;
		t->size = 0;
		return _delItem(i);
	}
	else
	{
		DequeItem_t* i = t->front;
		t->front = i->next;
		t->size--;
		return _delItem(i);
	}
}

void* Deque_at(Deque_t* t, uint32_t i)
{
	if (i >= t->size) return NULL;
	DequeItem_t* di = t->front;
	while (i--)
		di = di->next;
	return di->p;
}

bool Deque_contains(Deque_t* t, void* p)
{
	bool found = false;
	DequeItem_t* i;

	for (i = t->front; i != NULL; i = i->next)
	{
		if (i->p == p)
		{
			found = true;
			break;
		}
	}

	return found;
}

void* Deque_toArray(Deque_t* t)
{
	if (t->size == 0) return NULL;
	
	uint32_t i;
	DequeItem_t* it = t->front;
	uint32_t* a = (uint32_t*)malloc(sizeof(void*) * t->size);
	
	for (i = 0; i < t->size; i++)
	{
		a[i] = (uint32_t)it->p;
		it = it->next;
	}

	return a;
}

void Deque_appendDeque(Deque_t* t, Deque_t* other)
{
	DequeItem_t* di;

	di = Deque_front(other);
	while (di)
	{
		Deque_pushBack(t, Deque_itemDeref(di));
		di = Deque_next(di);
	}
}

DequeItem_t* Deque_front(Deque_t* t) { return t->front; }
DequeItem_t* Deque_back(Deque_t* t) { return t->back; }
DequeItem_t* Deque_next(DequeItem_t* iter) { return iter->next; }
DequeItem_t* Deque_prev(DequeItem_t* iter) { return iter->prev; }
void* Deque_itemDeref(DequeItem_t* iter) { return iter->p; }
size_t Deque_size(Deque_t* t) { return t->size; }
