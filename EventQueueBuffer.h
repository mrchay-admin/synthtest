#pragma once

#ifndef RSMESSAGE_QUEUE_DOUBLE_BUFFER
#define RSMESSAGE_QUEUE_DOUBLE_BUFFER

#include "EventQueue.h"

typedef struct EventQueueBuffer_t EventQueueBuffer_t;

EventQueueBuffer_t* EventQueueBuffer_create(uint32_t bufferSize);
void EventQueueBuffer_destroy(EventQueueBuffer_t* t);

EventQueue_t* EventQueueBuffer_getBuffer(EventQueueBuffer_t* t);
EventQueue_t* EventQueueBuffer_flip(EventQueueBuffer_t* t);






#endif