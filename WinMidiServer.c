#ifdef WIN32

#include "WinMidiServer.h"
#pragma comment(lib,"winmm.lib")
#include <Windows.h>
#include <mmsystem.h>

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#include "Deque.h"

#ifndef MIN
#define MIN(A,B) ((A)>(B)?(B):(A))
#endif
#ifndef MAX
#define MAX(A,B) ((A)>(B)?(A):(B))
#endif

typedef struct _WinMidiServer_t
{
	uint32_t count;
	Deque_t* names;

	HMIDIIN hMidiDevice;
	WinMidiServer_cb cb;
	void* privateTag;
} WinMidiServer_t;

static WinMidiServer_t g_wms;


uint32_t _getDevName(long lID, char* pszDeviceName, long lLen)
{
	int nRet;
	MIDIINCAPSA tagMIDIInCaps;
	
	memset(&tagMIDIInCaps, 0, sizeof(MIDIINCAPSA));
	nRet = midiInGetDevCapsA(lID, &tagMIDIInCaps, sizeof(MIDIINCAPSA));
	if (nRet != MMSYSERR_NOERROR)
	{
		return 0;
	}
	memset(pszDeviceName, 0, lLen);
	lLen = MIN(lLen - 1, (long)strlen(tagMIDIInCaps.szPname));
	strncpy(pszDeviceName, tagMIDIInCaps.szPname, lLen);
	return lLen;
}

void WinMidiServer_destroy()
{
	// stop midi if started
	WinMidiServer_stopDevice();

	// free name list
	if (g_wms.names)
	{
		DequeItem_t *del,* it = Deque_front(g_wms.names);
		while (it != NULL)
		{
			del = it;
			it = Deque_next(it);
			free(Deque_itemDeref(del));
		}
		Deque_destroy(g_wms.names);
	}
}

void WinMidiServer_init(WinMidiServer_cb cb,void* privateTag)
{
	uint32_t i;
	char* name;
	
	g_wms.cb = cb;
	g_wms.privateTag = privateTag;
	g_wms.count = midiInGetNumDevs();

	g_wms.names = Deque_create();
	
	for (i = 0; i < g_wms.count; i++)
	{
		name = malloc(256);
		if (0 == _getDevName(i, name, 255))
			free(name);
		else
			Deque_pushBack(g_wms.names, name);
	}
}

size_t WinMidiServer_getInputDeviceCount() { return g_wms.count;}
char*  WinMidiServer_getInputDeviceName(uint32_t i) { return Deque_at(g_wms.names, i); }

void OnShortData(DWORD dwInstance, DWORD dwParam1, DWORD dwParam2) 
{
	unsigned char cData[4];
	long lLen = 0;
	cData[0] = (unsigned char)(dwParam1 & 0x000000FF);
	cData[1] = (unsigned char)((dwParam1 & 0x0000FF00) >> 8);
	cData[2] = (unsigned char)((dwParam1 & 0x00FF0000) >> 16);
	cData[3] = (unsigned char)((dwParam1 & 0xFF000000) >> 24);

	if (0x80 <= cData[0] && cData[0] <= 0xBF ||
		0xE0 <= cData[0] && cData[0] <= 0xEF || cData[0] == 0xF2)
	{
		lLen = 3;
	}
	else if (0xC0 <= cData[0] && cData[0] <= 0xDF ||
		cData[0] == 0xF1 || cData[0] == 0xF3)
	{
		lLen = 2;
	}
	else if (cData[0] == 0xF6 || 0xF8 <= cData[0] && cData[0] <= 0xFF)
	{
		lLen = 1;
	}
	else
	{
		lLen = 0;
	}

	if (g_wms.cb)
		g_wms.cb(cData, lLen,g_wms.privateTag);
}

void CALLBACK MidiInProc(HMIDIIN hMidiIn, UINT wMsg, DWORD dwInstance, DWORD dwParam1, DWORD dwParam2)
{
	switch (wMsg) {
	case MIM_OPEN:  break;
	case MIM_CLOSE:	break;
	case MIM_DATA: OnShortData(dwInstance, dwParam1, dwParam2);	break;
	case MIM_LONGDATA:
		printf("wMsg=MIM_LONGDATA\n");
		break;
	case MIM_ERROR:
		printf("wMsg=MIM_ERROR\n");
		break;
	case MIM_LONGERROR:
		printf("wMsg=MIM_LONGERROR\n");
		break;
	case MIM_MOREDATA:
		printf("wMsg=MIM_MOREDATA\n");
		break;
	default:
		printf("wMsg = unknown\n");
		break;
	}
	return;
}

bool WinMidiServer_startDevice(uint32_t i)
{
	if (MMSYSERR_NOERROR != midiInOpen(&g_wms.hMidiDevice, i, (DWORD)(void*)MidiInProc, 0, CALLBACK_FUNCTION))
	{
		printf("midiInOpen() failed...\n");
		return false;
	}

	midiInStart(g_wms.hMidiDevice);

	return true;
}


void WinMidiServer_stopDevice()
{
	if (g_wms.hMidiDevice)
	{
		midiInStop(g_wms.hMidiDevice);
		midiInClose(g_wms.hMidiDevice);
		g_wms.hMidiDevice = NULL;
	}
}

#endif
