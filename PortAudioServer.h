#ifndef PORTSOUND_h
#define PORTSOUND_h

#include <stdint.h>

typedef void(*PortAudioServer_cb) (float* left, float* right, long bufferLen, void* privateTag);

void PortAudioServer_init(PortAudioServer_cb cb, void* ptag, uint32_t blockSize, uint32_t sampleRate);
void PortAudioServer_stop(void);

void PortAudioServer_cpu(void);

#endif