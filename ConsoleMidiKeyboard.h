#include <stdint.h>

typedef void(*midiEventCb)(uint8_t* data, size_t len, void* privateTag);

void ConsoleKeyboard(midiEventCb cb, void* tag);